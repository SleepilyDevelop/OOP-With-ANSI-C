#include <stdio.h>
#include <stdlib.h>

#include "Atom.h"
#include "new.h"

int main ()
{
	void * a = new(String, "alegro"), * aa = clone(a);
	void * b = new(String, "blow");
	
	printf("sizeOf(a) == %zu\n", sizeOf(a));
	if (differ(a, b))
		puts("ok");
	
	if (differ(a, aa))
		puts("differ?");
	
	if (a == aa)
		puts("clone?");
	
	print(a);
	print(b);
	
	delete(a), delete(aa), delete(b);
	
	return 0;
}
