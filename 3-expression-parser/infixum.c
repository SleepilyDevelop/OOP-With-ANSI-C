/* infix driver */

#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <stdio.h>

#include "value.h"

struct Type {
    const char * name;
    char rank, rpar;
    void * (* new) (va_list ap);
    void (* exec) (const void * tree, int rank, int par);
    void (* delete) (void * tree);
};

void * new (const void * type, ...)
{
    va_list ap;
    void * result;
    
    assert(type && ((struct Type *) type) -> new);
    
    va_start(ap, type);
    result = ((struct Type *) type) -> new(ap);
    * (const struct Type **) result = type;
    va_end(ap);
    return result;
}

static void exec (const void * tree, int rank, int par)
{
    assert(tree && * (struct Type **) tree
        && (* (struct Type **) tree) -> exec);

    return (* (struct Type **) tree) -> exec(tree, rank, par);
}

void process (const void * tree)
{
    putchar('\t');
    exec(tree, 0, 0);
    putchar('\n'); 
}

void delete (void * tree)
{
    assert(tree && * (struct Type **) tree
        && (* (struct Type **) tree) -> delete);

    (* (struct Type **) tree) -> delete(tree);
}


/* Числовой узел */
struct Val {
    const void * type;
    double value;
};

static void * mkVal (va_list ap)
{
    struct Val * node = malloc(sizeof(struct Val));

    assert(node);
    node -> value = va_arg(ap, double);
    return node; 
}

static void doVal (const void * tree, int rank, int par)
{
    printf("%g", ((struct Val *) tree) -> value);
}


/* Бинарный оператор */
struct Bin {
    const void * type;
    void  * left, * right;
};

static void * mkBin (va_list ap)
{
    struct Bin * node = malloc(sizeof(struct Bin));

    assert(node);
    node -> left = va_arg(ap, void *);
    node -> right = va_arg(ap, void *);
    return node;
}

static void doBin (const void * tree, int rank, int par)
{
    const struct Type * type = * (struct Type **) tree;

    par = type -> rank < rank
            ||  (par && type -> rank == rank);

    if (par) putchar('(');
    exec(((struct Bin *) tree) -> left, type -> rank, 0);
    printf(" %s ", type -> name);
    exec(((struct Bin *) tree) -> right,
            type -> rank, type -> rpar);
    if (par) putchar(')');
}

static void freeBin (void * tree)
{
    delete(((struct Bin *) tree) -> left);
    delete(((struct Bin *) tree) -> right);
    free(tree);
}


/* Унарный оператор */
struct Un {
    const void * type;
    void * right;
};

static void * mkUn (va_list ap)
{
    struct Un * node = malloc(sizeof(struct Val));

    assert(node);
    node -> right = va_arg(ap, void *);
    return node;
}

static void doUn (const void * tree, int rank, int par)
{
    const struct Type * type = * (struct Type **) tree;

    par = type -> rank < rank
            ||  (par && type -> rank == rank);

    if (par) putchar('(');
    printf(" %s ", type -> name);
    exec(((struct Bin *) tree) -> right,
            type -> rank, type -> rpar);
    if (par) putchar(')');
}

static void freeUn (void * tree)
{
    delete(((struct Un *) tree) -> right);
    free(tree);
}

const struct Type _Add  = { "+", 1, 0, mkBin, doBin, freeBin };
const struct Type _Sub  = { "-", 1, 1, mkBin, doBin, freeBin };
const struct Type _Div  = { "/", 1, 1, mkBin, doBin, freeBin };
const struct Type _Mult = { "*", 1, 1, mkBin, doBin, freeBin };
const struct Type _Minus = { "neg", 1, 0, mkUn, doUn, freeUn };
const struct Type _Value = { "", 1, 0, mkVal, doVal, free};

const void * Add = & _Add;
const void * Sub = & _Sub;
const void * Div = & _Div;
const void * Mult = & _Mult;
const void * Minus = & _Minus;
const void * Value = & _Value;
const void * Factor = 0;
