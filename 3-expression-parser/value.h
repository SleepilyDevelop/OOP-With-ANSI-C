extern const void * Add;   /* Addition */
extern const void * Sub;   /* Subdivision */
extern const void * Div;   /* Division */
extern const void * Mult;  /* Multiplication */
extern const void * Minus; 
extern const void * Factor;
extern const void * Value;

void * new (const void * type, ...);
void process (const void *tree);
void delete (void * tree);
