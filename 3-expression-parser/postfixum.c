#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <stdio.h>

#include "value.h"

struct Type {
    const char * name;
    void * (* new) (va_list ap);
    void (* exec) (const void * tree);
    void (* delete) (void * tree);
};

void * new (const void * type, ...)
{
    va_list ap;
    void * result;
    
    assert(type && ((struct Type *) type) -> new);
    
    va_start(ap, type);
    result = ((struct Type *) type) -> new(ap);
    * (const struct Type **) result = type;
    va_end(ap);
    return result;
}

static void exec (const void * tree)
{
    assert(tree && * (struct Type **) tree
        && (* (struct Type **) tree) -> exec);

    return (* (struct Type **) tree) -> exec(tree);
}

void process (const void * tree)
{
    putchar('\t');
    exec(tree);
    putchar('\n'); 
}

void delete (void * tree)
{
    assert(tree && * (struct Type **) tree
        && (* (struct Type **) tree) -> delete);

    (* (struct Type **) tree) -> delete(tree);
}


/* Числовой узел */
struct Val {
    const void * type;
    double value;
};

static void * mkVal (va_list ap)
{
    struct Val * node = malloc(sizeof(struct Val));

    assert(node);
    node -> value = va_arg(ap, double);
    return node; 
}

static void doVal (const void * tree)
{
    printf(" %g", ((struct Val *) tree) -> value);
}


/* Бинарный оператор */
struct Bin {
    const void * type;
    void  * left, * right;
};

static void * mkBin (va_list ap)
{
    struct Bin * node = malloc(sizeof(struct Bin));

    assert(node);
    node -> left = va_arg(ap, void *);
    node -> right = va_arg(ap, void *);
    return node;
}

static void doBin (const void * tree)
{
    exec(((struct Bin *) tree) -> left);
    exec(((struct Bin *) tree) -> right);
    printf(" %s", (* (struct Type **) tree) -> name);
}

static void freeBin (void * tree)
{
    delete(((struct Bin *) tree) -> left);
    delete(((struct Bin *) tree) -> right);
    free(tree);
}


/* Унарный оператор */
struct Un {
    const void * type;
    void * left;
};

static void * mkUn (va_list ap)
{
    struct Un * node = malloc(sizeof(struct Val));

    assert(node);
    node -> left = va_arg(ap, void *);
    return node;
}

static void doUn (const void * tree)
{
    exec(((struct Un *) tree) -> left);
    printf(" %s", (* (struct Type **) tree) -> name);
}

static void freeUn (void * tree)
{
    delete(((struct Un *) tree) -> left);
    free(tree);
}

const struct Type _Add  = { "+", mkBin, doBin, freeBin };
const struct Type _Sub  = { "-", mkBin, doBin, freeBin };
const struct Type _Div  = { "/", mkBin, doBin, freeBin };
const struct Type _Mult = { "*", mkBin, doBin, freeBin };
const struct Type _Minus = { "negative", mkUn, doUn, freeUn };
const struct Type _Value = { "", mkVal, doVal, free};

const void * Add = & _Add;
const void * Sub = & _Sub;
const void * Div = & _Div;
const void * Mult = & _Mult;
const void * Minus = & _Minus;
const void * Value = & _Value;
const void * Factor = 0;
